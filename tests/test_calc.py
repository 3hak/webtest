"""
@author: Nikolay Z.
Created 2017/07/11
"""

from itertools import product
from time import sleep

import pytest
from hamcrest import assert_that, none, is_, all_of, greater_than

from utils import idparametrize
from p_obj.calc import CalcPage

BROWSERS = ['firefox', 'chrome', 'phantomjs']
INCORRECT_VALUES = [0, -1, -2**32, "some text value", 0.01, 0.001]
CORRECT_VALUES = [1, 100, 1000, 2**32]


def transform_result_value(string_value):
    """Can't just strip all non-digits due to separator in string"""
    if "=" in string_value:
        string_value = string_value[:-5]
    else:
        string_value = string_value[:-4]
    return float(string_value.strip().replace(",", ".").replace(" ", ""))


class CaseCalc(object):
    """Testcase class; it holds all required test data; one instance per one case"""
    def __init__(self, browser, value, expect_result):
        self.browser = browser
        self.value = value
        self.expect_result = expect_result

    def __repr__(self):
        return "Browser: {}, Value: {}, Expect result: {}".format(self.browser, self.value, self.expect_result)

CORRECT_CASES = [CaseCalc(browser, value, True) for (browser, value) in product(BROWSERS, CORRECT_VALUES)]
INCORRECT_CASES = [CaseCalc(browser, value, False) for (browser, value) in product(BROWSERS, INCORRECT_VALUES)]

class TestCalc(object):
    """Test for calculator on-site"""

    @idparametrize('case', CORRECT_CASES + INCORRECT_CASES)
    def test_calculator_input_value(self, case, browser):
        """Test calculator for input values versus form output"""
        if case.browser == "chrome":
            pytest.xfail("Chromedriver is broken in my configuration :(")

        driver = browser.get(case.browser)
        calc_page = CalcPage(driver)
        calc_page.input_to_field(case.value)
        calc_page.send_form()
        sleep(2)  # Calculator page is slow in comparison to interpreter
        result_from, result_to = calc_page.get_calc_result(expect_absense = not case.expect_result)

        if not case.expect_result:
            with pytest.allure.step("Check empty response"):  # @UndefinedVariable
                assert_that(all_of([
                    result_from, is_(none()),
                    result_to, is_(none())]))
        else:
            with pytest.allure.step("Check numeral response"):  # @UndefinedVariable
                assert_that(all_of([
                    transform_result_value(result_to), is_(greater_than(0)),
                    transform_result_value(result_from), is_(greater_than(0))]))
