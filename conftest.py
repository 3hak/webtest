import pytest
import logging
from utils import initiate_ff, initiate_chrome, initiate_phantomjs  # @UnusedImport


@pytest.yield_fixture(scope='function')
def browser():
    driver = {
        "firefox": initiate_ff(),
#         "chrome": initiate_chrome(),  # Broken - crashes on start :(
        "phantomjs": initiate_phantomjs()
        }

    yield driver

    for dr in driver.itervalues():
        try:
            dr.quit()
        except OSError:
            logging.debug("Looks like browser was not started or already exited")
