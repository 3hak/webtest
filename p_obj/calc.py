from p_obj.page import Page
from p_obj.locators import CalcFormLocators
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import TimeoutException
import allure

CALC_URL = "http://www.sberbank.ru/ru/quotes/converter"


class CalcPage(Page):
    """More specific class for our calc"""
    def __init__(self, driver):
        super(CalcPage, self).__init__(driver)
        self.driver = driver
        self.driver.get(CALC_URL)

    @allure.step("Input test value to field")
    def input_to_field(self, value):
        """
            Input to the field
            :args:value: value to be entered
        """
        try:
            element = WebDriverWait(self.driver, 5).until(EC.presence_of_element_located(CalcFormLocators.INPUT_FIELD))
        except TimeoutException as e:
            print "Can't find Input Field!"
            raise e

        element.clear()
        element.send_keys(str(value))
        assert element

    @allure.step("Click Send button")
    def send_form(self):
        """Click on send form button"""
        try:
            element = WebDriverWait(self.driver, 5).until(EC.presence_of_element_located(CalcFormLocators.FORM_APPROVE))
        except TimeoutException as e:
            print "Can't find Send Form Button!"
            raise e
        element.click()

    @allure.step("Get calculation form result")
    def get_calc_result(self, expect_absense=False):
        """
            Get result from bottom of the form.
            :args:expect_absense: expect absense of form, because it's been rendered only for valid calculation in first place
        """
        result = [None, None]
        try:
            result_from_elem = WebDriverWait(self.driver, 5).until(EC.presence_of_element_located(CalcFormLocators.RESULT_FROM))
            result_to_elem = WebDriverWait(self.driver, 5).until(EC.presence_of_element_located(CalcFormLocators.RESULT_TO))
            result = [result_from_elem.text, result_to_elem.text]
        except TimeoutException as e:
            print "There's no result for calculation in form"
            if not expect_absense:
                raise e
            else:
                pass

        return result
