# -*- coding: utf-8 -*-
from selenium.webdriver.common.by import By

INPUT_XPATH = """.//*[@placeholder="Сумма"]"""

class CalcFormLocators(object):
    INPUT_FIELD = (By.XPATH, INPUT_XPATH)
    FORM_APPROVE = (By.CLASS_NAME, "rates-button")
    RESULT_FROM = (By.CLASS_NAME, "rates-converter-result__total-from")
    RESULT_TO = (By.CLASS_NAME, "rates-converter-result__total-to")
