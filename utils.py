import pytest
from selenium import webdriver

def idparametrize(name, values, fixture=False):
    return pytest.mark.parametrize(name, values, ids=map(repr, values), indirect=fixture)


def initiate_ff():
    return webdriver.Firefox()


def initiate_chrome():
    return webdriver.Chrome()


def initiate_phantomjs():
    return webdriver.PhantomJS()
